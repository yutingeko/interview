const formSetting = {
  name: {
    attr: {
      type: "text",
      placeholder: "Your Full Name *"
    },
    validation: {
      rule: "EnglishOnly",
      required: true
    }
  },
  email: {
    attr: {
      type: "text",
      placeholder: "Your Email *"
    },
    validation: {
      rule: "Email",
      required: true
    }
  },
  phone: {
    attr: {
      type: "text",
      placeholder: "Your Phone *"
    },
    validation: {
      rule: "Phone",
      required: true
    }
  },
  message: {
    attr: {
      type: "textarea",
      placeholder: "Message"
    },
    validation: {
      required: false
    }
  }
};

const validationRules = {
  EnglishOnly: RegExp(/^[a-zA-Z]+$/),
  Email: RegExp(/\S+@\S+\.\S+/),
  Phone: RegExp(/^\d{8,10}$/)
}

export { formSetting, validationRules };